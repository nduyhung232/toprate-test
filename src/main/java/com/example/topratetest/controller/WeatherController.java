package com.example.topratetest.controller;

import com.example.topratetest.model.WeatherDay;
import com.example.topratetest.service.IWeatherService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequiredArgsConstructor
@RestController
public class WeatherController {

    private final IWeatherService iWeatherService;

    @GetMapping("/weather/forecast/5day")
    public ResponseEntity<List<WeatherDay>> weatherForecast5Day() {
        List<WeatherDay> weatherDays = iWeatherService.weatherForecast5Day();
        return ResponseEntity.ok(weatherDays);
    }
}
