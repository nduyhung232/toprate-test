package com.example.topratetest.repository;

import com.example.topratetest.entity.WeatherEnt;
import com.example.topratetest.entity.WeatherId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WeatherRepo extends JpaRepository<WeatherEnt, WeatherId> {

}
