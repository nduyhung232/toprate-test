package com.example.topratetest.openapi.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class Response {
    @JsonProperty("cod")
    private String code;

    @JsonProperty("message")
    private String message;

    @JsonProperty("cnt")
    private String cnt;

    @JsonProperty("list")
    private List<WeatherDay> list;

}
