package com.example.topratetest.openapi.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class WeatherDay {
    @JsonProperty("main")
    private WeatherDetail weatherDetails;

    @JsonProperty("dt_txt")
    private String dateTime;
}
