package com.example.topratetest.openapi.impl;

import com.example.topratetest.exception.UnprocessableEntityException;
import com.example.topratetest.openapi.IWeatherOpenApi;
import com.example.topratetest.openapi.dto.Response;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@RequiredArgsConstructor
@Service
public class WeatherOpenApi implements IWeatherOpenApi {

    @Value("${weather.openapi.url}")
    private String weatherOpenApi;

    private final RestTemplate restTemplate;

    @Override
    public Response fetchWeather() {
        try {
            ResponseEntity<Response> result =
                    restTemplate.getForEntity(weatherOpenApi, Response.class);

            return result.getBody();
        } catch (HttpClientErrorException exception) {
            throw new UnprocessableEntityException("Has problem when connect to weather open api");
        }
    }
}
