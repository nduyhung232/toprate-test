package com.example.topratetest.openapi;

import com.example.topratetest.openapi.dto.Response;

public interface IWeatherOpenApi {

    Response fetchWeather();
}
