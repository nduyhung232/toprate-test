package com.example.topratetest.service.impl;

import com.example.topratetest.entity.WeatherEnt;
import com.example.topratetest.entity.WeatherId;
import com.example.topratetest.exception.NotFoundException;
import com.example.topratetest.model.WeatherDay;
import com.example.topratetest.model.WeatherHour;
import com.example.topratetest.openapi.IWeatherOpenApi;
import com.example.topratetest.openapi.dto.Response;
import com.example.topratetest.repository.WeatherRepo;
import com.example.topratetest.service.IWeatherService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Service
public class WeatherService implements IWeatherService {
    private final IWeatherOpenApi iWeatherOpenApi;
    private final WeatherRepo weatherRepo;

    @Override
    public List<WeatherDay> weatherForecast5Day() {
        // fetch data
        Response response = iWeatherOpenApi.fetchWeather();

        // validate
        if (response == null || response.getList() == null || response.getList().size() == 0) {
            throw new NotFoundException("fetch data weather is null");
        }

        // save to db
        List<WeatherEnt> weatherEnts = new ArrayList<>();
        for (int i = 0; i < response.getList().size(); i++) {
            com.example.topratetest.openapi.dto.WeatherDay weatherDay = response.getList().get(i);

            WeatherId weatherId = new WeatherId();
            weatherId.setDate(weatherDay.getDateTime().split(" ")[0]);
            weatherId.setTime(weatherDay.getDateTime().split(" ")[1]);

            WeatherEnt weatherEnt = new WeatherEnt();
            weatherEnt.setWeatherId(weatherId);
            weatherEnt.setTemp(weatherDay.getWeatherDetails().getTemp());
            weatherEnt.setTempMax(weatherDay.getWeatherDetails().getTempMax());
            weatherEnt.setTempMin(weatherDay.getWeatherDetails().getTempMin());

            weatherEnts.add(weatherEnt);
        }
        weatherRepo.saveAll(weatherEnts);

        /**
         *  looking for the coldest/hottest time of the day
          */
        List<WeatherDay> weatherDays = new ArrayList<>();
        String curDate = null;
        for (int i = 0; i < weatherEnts.size(); i++) {
            WeatherEnt weatherEnt = weatherEnts.get(i);

            WeatherHour weatherHour = WeatherHour.builder()
                    .hour(weatherEnt.getWeatherId().getTime())
                    .temp(weatherEnt.getTemp())
                    .build();

            if (weatherEnt.getWeatherId().getDate().equals(curDate)) {
                curDate = weatherEnt.getWeatherId().getDate();

                WeatherDay curWeatherDay = weatherDays.get(weatherDays.size() - 1);
                curWeatherDay.getWeatherHour().add(weatherHour);

                if (curWeatherDay.getTempOfWarmest() < weatherHour.getTemp()) {
                    curWeatherDay.setTempOfWarmest(weatherHour.getTemp());
                    curWeatherDay.setHourOfWarmest(weatherHour.getHour());
                }

                if (curWeatherDay.getTempOfColdest() > weatherHour.getTemp()) {
                    curWeatherDay.setTempOfColdest(weatherHour.getTemp());
                    curWeatherDay.setHourOfColdest(weatherHour.getHour());
                }
            } else {
                curDate = weatherEnt.getWeatherId().getDate();

                WeatherDay weatherDay = WeatherDay.builder()
                        .date(weatherEnt.getWeatherId().getDate())
                        .tempOfWarmest(weatherEnt.getTemp())
                        .hourOfWarmest(weatherEnt.getWeatherId().getTime())
                        .tempOfColdest(weatherEnt.getTemp())
                        .hourOfColdest(weatherEnt.getWeatherId().getTime())
                        .weatherHour(new ArrayList<WeatherHour>() {{
                                add(weatherHour);
                            }})
                        .build();

                weatherDays.add(weatherDay);
            }
        }

        // build message
        weatherDays.stream().forEach(obj -> obj.buildMessage());

        return weatherDays;
    }
}
