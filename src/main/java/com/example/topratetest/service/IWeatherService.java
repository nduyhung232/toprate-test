package com.example.topratetest.service;

import com.example.topratetest.model.WeatherDay;

import java.util.List;

public interface IWeatherService {
    List<WeatherDay> weatherForecast5Day();
}
