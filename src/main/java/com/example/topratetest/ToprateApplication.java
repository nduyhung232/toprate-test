package com.example.topratetest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ToprateApplication {

  public static void main(String[] args) {
    SpringApplication.run(ToprateApplication.class, args);
  }

}
