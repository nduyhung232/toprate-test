package com.example.topratetest.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "weather")
public class WeatherEnt {
    @EmbeddedId
    private WeatherId WeatherId;

    @Column(name = "temp")
    private Float temp;

    @Column(name = "temp_min")
    private Float tempMin;

    @Column(name = "temp_max")
    private Float tempMax;
}
