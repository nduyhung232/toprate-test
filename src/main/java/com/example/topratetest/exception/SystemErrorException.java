package com.example.topratetest.exception;

public class SystemErrorException extends RuntimeException{
  public SystemErrorException(String message) {
    super(message);
  }
}
