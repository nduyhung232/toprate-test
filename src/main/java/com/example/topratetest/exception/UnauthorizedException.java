package com.example.topratetest.exception;

public class UnauthorizedException extends RuntimeException{
  public UnauthorizedException(String message) {
    super(message);
  }
}
