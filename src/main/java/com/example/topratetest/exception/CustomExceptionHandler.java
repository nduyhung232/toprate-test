package com.example.topratetest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

@RestControllerAdvice
public class CustomExceptionHandler {
  @ExceptionHandler(SystemErrorException.class)
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  public ErrorResponse handSystemErrorException(SystemErrorException systemErrorException, WebRequest webRequest) {
    return new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, systemErrorException.getMessage());
  }

  @ExceptionHandler(NotFoundException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  public ErrorResponse handNotFoundException(NotFoundException notFoundException, WebRequest webRequest) {
    return new ErrorResponse(HttpStatus.NOT_FOUND, notFoundException.getMessage());
  }

  @ExceptionHandler(UnauthorizedException.class)
  @ResponseStatus(HttpStatus.UNAUTHORIZED)
  public ErrorResponse handUnauthorizedException(UnauthorizedException unauthorizedException, WebRequest webRequest) {
    return new ErrorResponse(HttpStatus.UNAUTHORIZED, unauthorizedException.getMessage());
  }

  @ExceptionHandler(UnprocessableEntityException.class)
  @ResponseStatus(HttpStatus.UNAUTHORIZED)
  public ErrorResponse handUnprocessableEntityException(UnprocessableEntityException unauthorizedException, WebRequest webRequest) {
    return new ErrorResponse(HttpStatus.UNPROCESSABLE_ENTITY, unauthorizedException.getMessage());
  }
}
