package com.example.topratetest.exception;

public class UnprocessableEntityException extends RuntimeException{
  public UnprocessableEntityException(String message) {
    super(message);
  }
}
