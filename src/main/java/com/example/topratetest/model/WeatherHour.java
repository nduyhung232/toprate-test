package com.example.topratetest.model;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class WeatherHour {
    private String hour;
    private Float temp;
}
