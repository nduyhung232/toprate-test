package com.example.topratetest.model;

import lombok.Builder;
import lombok.Data;

import java.util.ArrayList;

@Builder
@Data
public class WeatherDay {
    private String date;
    private String hourOfWarmest;
    private Float tempOfWarmest;
    private String hourOfColdest;
    private Float tempOfColdest;
    private ArrayList<WeatherHour> weatherHour;
    private String message;

    public void buildMessage() {
        String message = "%s: %s is the coldest hour(%s k), %s is the warmest hour(%s k)";
        this.message = String.format(message,
                this.date,
                this.hourOfColdest,
                this.tempOfColdest,
                this.hourOfWarmest,
                this.tempOfWarmest
                );

    }
}
